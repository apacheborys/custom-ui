import {
    Component,
    Input,
    NgModule,
    OnInit
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Language } from 'angular-l10n';

@Component({
    selector: 'categories',
    template: require('./categories.component.html'),
    styles:   [require('./categories.component.scss')],
})

@NgModule({
    imports:      [
        CommonModule,
    ],
    providers:    [],
    declarations: []
})

export class CategoriesComponent implements OnInit
{
    @Language()
    public lang:string;

    @Input()
    public myTitle:string;

    constructor()
    {
    }

    public ngOnInit():void
    {
    }

    public redirectToDevelopersTutorial():void
    {
        this.callToUrl('https://'+window.location.hostname + '/rest/markets/manomano/actions/categories/all', "GET");
        console.log('request');
        // var data = new FormData();
        // data.append('manomano', 'test');
        // this.callToUrl('https://'+window.location.hostname + '/rest/markets/manomano/actions/test',"POST", data);
        // console.log('request');
    }

    public callToUrl(theUrl, method, data = null)
    {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open( method, theUrl, true ); // false for synchronous request
        xmlHttp.setRequestHeader('Accept', 'application/json');
        xmlHttp.setRequestHeader('Authorization', 'Bearer '+localStorage.getItem('accessToken'));
        xmlHttp.send( data );
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState != 4) return;

            console.log(xmlHttp.responseText);
        }
    }
}
