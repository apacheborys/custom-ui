import {
    Component,
    Input,
    NgModule,
    OnInit
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Language } from 'angular-l10n';
import {
    TerraSelectBoxValueInterface
} from '@plentymarkets/terra-components';

@Component({
    selector: 'start',
    template: require('./start.component.html'),
    styles:   [require('./start.component.scss')],
})

@NgModule({
    imports:      [
        CommonModule,
    ],
    providers:    [],
    declarations: []
})

export class StartComponent implements OnInit
{
    private _login:string;
    private _password:string;
    private _stateSelection:Array<TerraSelectBoxValueInterface>;

    private _manomanoLanguageSelect:Array<TerraSelectBoxValueInterface> = [];
    private _pickedManomanoLanguage:string;

    @Language()
    public lang:string;

    @Input()
    public myTitle:string;

    constructor()
    {
    }

    public ngOnInit():void
    {
        this._manomanoLanguageSelect.push(
            {
                value:   'fr',
                caption: 'France'
            },
            {
                value:   'de',
                caption: 'Germany'
            },
            {
                value:   'es',
                caption: 'Spain'
            },
            {
                value:   'it',
                caption: 'Italia'
            },
            {
                value:   'uk',
                caption: 'United Kingdom'
            },
        );

        this._pickedManomanoLanguage = 'fr'

        this._stateSelection = [];
        this._stateSelection.push(
            {
                value:   'login',
                caption: 'Please enter your manomano login'
            },
            {
                value:   'password',
                caption: 'Please enter your manomano password'
            }
        );
    }

    public saveManomanoCredentials():void
    {
        var data = new FormData();
        data.append('manomano', this._login + ' ' + this._password);
        console.log('request');
        this.callToUrl('https://'+window.location.hostname + '/rest/markets/manomano/actions/test',"POST", data);
    }

    public onSelectManomanoLanguage(event):void
    {
        console.log('catch: ' + this._pickedManomanoLanguage);
        console.log('event: ');
        console.log(event);
    }

    public callToUrl(theUrl, method, data = null)
    {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open( method, theUrl, true ); // false for synchronous request
        xmlHttp.setRequestHeader('Accept', 'application/json');
        xmlHttp.setRequestHeader('Authorization', 'Bearer '+localStorage.getItem('accessToken'));
        xmlHttp.send( data );
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState != 4) return;

            console.log(xmlHttp.responseText);
        }
    }
}
