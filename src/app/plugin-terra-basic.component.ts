import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
    selector: 'plugin-terra-basic-app',
    template: require('./plugin-terra-basic.component.html'),
    styles:   [require('./plugin-terra-basic.component.scss')],
})
export class PluginTerraBasicComponent
{
    private _action:any;

    constructor()
    {

        this._action = this.getUrlVars()['action'];

    }

    private getUrlVars()
    {
        let vars = {};

        window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(substring:string, ...args:any[]):string
        {
            vars[args[0]] = args[1];
            return;
        });

        return vars;
    }


}
